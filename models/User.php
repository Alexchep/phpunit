<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class User
 * @package app\models
 * @property string $username
 * @property string $password
 */
class User extends ActiveRecord
{
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password'], 'string', 'max' => 255],
            ['username', 'match', 'pattern' => '#^[a-z0-9_-]+$#i'],
            ['username', 'unique'],
        ];
    }
}