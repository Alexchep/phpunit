<?php

namespace tests\unit;

use Yii;
use app\models\User;

class UserTest extends \PHPUnit_Extensions_Database_TestCase
{
    protected function getConnection()
    {
        $pdo = new \PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        return $this->createDefaultDBConnection($pdo, $GLOBALS['DB_DBNAME']);
    }


    protected function getDataSet()
    {
        return $this->createXMLDataSet(dirname(__FILE__) . '/../_data/users.xml');
    }


    public function testValidateEmptyValues()
    {
        $user = new User();

        $this->assertFalse($user->validate(), 'model is not valid');
        $this->assertArrayHasKey('username', $user->getErrors(), 'check username error');
        $this->assertArrayHasKey('password', $user->getErrors(), 'check password error');
    }


    public function testValidateWrongValues()
    {
        $user = new User([
            'username' => 'Wrong % username',
            'password' => 123
        ]);

        $this->assertFalse($user->validate(), 'validate incorrect username or password');
        $this->assertArrayHasKey('username', $user->getErrors(), 'check incorrect username error');
        $this->assertArrayHasKey('password', $user->getErrors(), 'check incorrect password error');
    }


    public function testValidateCorrectValues()
    {
        $user = new User([
            'username' => 'Username3',
            'password' => '3e3e3e'
        ]);

        $this->assertTrue($user->validate(), 'correct model is valid');
    }


    public function testValidateExistedValues()
    {
        $user = new User([
            'username' => 'Alex',
            'password' => '1q1q1q'
        ]);

        $this->assertFalse($user->validate(), 'model is not valid');
        $this->assertArrayHasKey('username', $user->getErrors(), 'check existed username error');
    }


    public function testSaveIntoDb()
    {
        $user = new User([
            'username' => 'Username4',
            'password' => '4r4r4r'
        ]);

        $this->assertTrue($user->save(), 'model saved');
    }
}