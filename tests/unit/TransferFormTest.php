<?php

namespace tests\unit;

use app\models\TransferForm;
use PHPUnit\Framework\TestCase;

class TransferFormTest extends TestCase
{
    /**
     * @dataProvider validData
     *
     * @param $amount
     * @param $currency
     */
    public function testValidation($amount, $currency)
    {
        $model = new TransferForm();

        $model->amount = $amount;
        $model->currency = $currency;

        $this->assertNotEmpty($model->amount, 'amount can not be empty');
        $this->assertInternalType('numeric', $model->amount, 'amount must be a numeric');

        $this->assertNotEmpty($model->currency, 'currency can not be empty');
        $this->assertArrayHasKey($model->currency, array_flip(['USD', 'EUR', 'UAH', 840, 978, 980]), 'currency is denied');
    }


    /**
     * @return array
     */
    public function validData()
    {
        return [
            ['amount' => 99, 'currency' => 'USD'],
            ['amount' => '99', 'currency' => 840]
        ];
    }
}