<?php

namespace tests\unit;

use PHPUnit\Framework\TestCase;
use yii\base\Exception;

class FutureTest extends TestCase
{
    /**
     * @group future
     */
    public function testSomeFuture()
    {
        $this->markTestIncomplete();
    }

    /**
     * @expectedException Exception
     */
    public function testExceptionCase()
    {
        return 2 / 0;
    }
}